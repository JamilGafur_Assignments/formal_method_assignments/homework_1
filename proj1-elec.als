--===============================================
-- CS:5810 Formal Methods in Software Engineering
-- Fall 2020
--
-- Mini Project 1
--
-- Name: Jamil Gafur, Alex Hubers
--
-- ## Forward: A note about statuses and their semantics.
--
-- When you introduce a state transition system, you also introduce an implicit specification that:
-- 1) each Process with a given Status label meets the semantic criteria of that Status, and
-- 2) Each Process meeting the semantic criteria of a given Status is labelled accordingly.

-- One way to enforce this specification would be to validate as preconditions that each
-- given process passed to a given Operator meets the above semantic expectations. E.g,
-- to check in the predicate run_[p, pr] that p has memory, code, but lacks a processor.

-- Alternatively, we deliberately choose not to validate these preconditions within our Operators,
-- outside of it having the correct Status. The combination of `init` and `trans` make
-- such checks redundant.
--===============================================

abstract sig Resource {}
  
sig MemBlock, Processor, OtherResource extends Resource {} 
sig File in OtherResource {}

enum Status { BlockedIn, BlockedOut, Inactive, Running, WaitingIn, WaitingOut }

sig Process {
  var code: lone MemBlock,
  var mem: lone MemBlock,  
  var proc: lone Processor,
  var res: lone OtherResource,
  var status: Status
}

enum Operator { Block, Create, Preempt, Resume, Run, Suspend, Terminate, Unblock }
one sig Track { var op: lone Operator }


------------------------
-- Auxiliary predicates
------------------------

pred isBlocked [p: Process] {
  p.status in BlockedIn + BlockedOut
}

pred isWaiting [p: Process] {
  p.status in WaitingIn +  WaitingOut
}

pred isInMemory [p: Process] {
  p.status in WaitingIn + BlockedIn + Running
}

pred canSuspend[p: Process] {
  -- Note: We added this predicate.
  p.status in WaitingIn + BlockedIn
}

pred isSuspended [p: Process] {
  p.status in WaitingOut + BlockedOut
}

pred isRunning [p: Process] {
  p.status = Running
}

pred isInactive [p: Process] {
  p.status = Inactive
}

pred isSwappedOut [p: Process] {
  p.status in BlockedOut + WaitingOut
}

pred overlap [m1: MemBlock, m2: MemBlock] {
  m1 = m2
}

-----------------------------
-- Frame condition predicate
-----------------------------

pred noChangeExcept [r: Process -> univ, P: set Process ] {
  all p: Process - P | p.r' = p.r
}

-------------
-- Operators
-------------

/* Leave the constraints on Track.op' to track the applied
   operator in each state of a trace  
*/

-- block
pred block [p: Process, r: OtherResource ] {
  -- preconditions
  isRunning[p]

  -- post-conditions
  p.proc' = none
  p.res' = r
  p.status' = BlockedIn

  -- frame-conditions
  noChangeExcept[proc, p]
  noChangeExcept[res, p]
  noChangeExcept[status, p]
  noChangeExcept[mem, none]
  noChangeExcept[code, none]

  Track.op' = Block
}

-- create
pred create [p: Process, c: MemBlock, m: MemBlock] {  
  --preconditions
	isInactive[p]

	-- As per specifications:
	-- The code memory block must not be already used for data by some other active process. 
	no mem.c
	-- The data memory block must not be used all all by other active processes.
	no code.m and no mem.m
	-- Note: We're also going to enforce these MemBlocks be distinct, although
	-- the specification omits this requirement. This could constitute an over-constraint
	-- of the model. But this is a specification I would argue to the customer need be necessary.
	not overlap[c, m]
  
  -- post conditions
  p.mem' = m
  p.code' = c
  p.proc' = none
  p.res' = none
  p.status' = WaitingIn
  
  -- Frame Conditions
  noChangeExcept[mem, p]
  noChangeExcept[code, p]
  noChangeExcept[proc, p]
  noChangeExcept[res, p]
  noChangeExcept[status, p]

  Track.op' = Create
}

-- preempt
pred preempt [p: Process] {
  -- preconditions
  isRunning[p]

  -- post-conditions
  p.proc' = none
  p.status' = WaitingIn

  -- frame-conditions
  noChangeExcept[proc, p]
  noChangeExcept[status, p]
  noChangeExcept[mem, none]
  noChangeExcept[code, none]
  noChangeExcept[res, none]

  Track.op' = Preempt
}

-- resume
pred resume [p: Process, c: MemBlock, m: MemBlock] 
{    
  -- preconditions
  isSwappedOut[p]
  -- the two memory blocks must be distinct  
  not overlap[c, m]
	-- The code memory block must not be already used for data by some other active process. 
	no mem.c
	-- The data memory block must not be used all all by other active processes.
	no code.m and no mem.m
  
  -- post-conditions  
  p.status = BlockedOut implies p.status' = BlockedIn else p.status' = WaitingIn
  p.code' = c and p.mem' = m 

  -- frame-conditions
  noChangeExcept[status, p]
  noChangeExcept[mem, p]
  noChangeExcept[code, p]
  noChangeExcept[res, none]
  noChangeExcept[proc, none]

  Track.op' = Resume
}

-- run_
pred run_ [p: Process, pr: Processor] {
  -- preconditions
  p.status = WaitingIn
  -- The Processor must not be in use.
  no proc.pr

  -- post-conditions
  p.proc' = pr
  p.status' = Running

  -- frame-conditions
  noChangeExcept[proc, p]
  noChangeExcept[status, p]
  noChangeExcept[mem, none]
  noChangeExcept[code, none]
  noChangeExcept[res, none]

  Track.op' = Run
}

-- suspend
pred suspend [p: Process] {
  -- pre-conditions
  canSuspend[p]

  -- post-conditions  
  p.status = BlockedIn implies p.status' = BlockedOut else p.status' = WaitingOut
  p.code' = none and p.mem' = none 

  -- frame-conditions
  noChangeExcept[status, p]
  noChangeExcept[mem, p]
  noChangeExcept[code, p]
  noChangeExcept[res, none]
  noChangeExcept[proc, none]

  Track.op' = Suspend
}

-- terminate
pred terminate [p: Process] {
  -- preconditions
  isRunning[p]
  
  -- post conditions
  p.code' = none
  p.mem' = none
  p.proc' = none
  p.res' = none
  p.status' = Inactive
	
  -- frame conditions
  noChangeExcept[proc, p]
  noChangeExcept[status, p]
  noChangeExcept[mem, p]
  noChangeExcept[code, p]
  noChangeExcept[res, p]

  Track.op' = Terminate
}

-- unblock
pred unblock [p: Process] {
  -- preconditions
  isBlocked[p]

  -- post-conditions
  p.status = BlockedIn implies p.status' = WaitingIn else p.status' = WaitingOut
  p.res' = none
  
  -- frame-conditions
  noChangeExcept[proc, none]
  noChangeExcept[res, p]
  noChangeExcept[status, p]
  noChangeExcept[mem, none] 
  noChangeExcept[code, none] 

  Track.op' = Unblock
}


--------------------------
-- Inital state condition
--------------------------
pred Init [] {
	-- All processes are inactive.
	all p: Process | p.status = Inactive

	-- No resources whatsoever are allocated to the processes.
	no (code + mem + proc + res)
}

-----------------------
-- Transition relation
-----------------------
pred Trans [] {
  some p: Process | 
       (some r: OtherResource | block [p, r])
    or (some c, m: MemBlock | create [p, c, m])
    or preempt [p]
    or (some c, m: MemBlock | resume [p, c, m])
    or (some pr: Processor | run_ [p, pr])
    or suspend [p]
    or terminate [p]
    or unblock [p]
}

-------------
-- Scheduler
-------------

-- All traces are according to the scheduler
fact Scheduler {
  Init and always Trans

  Track.op = none
}

---------------------
-- Sanity check runs
---------------------

pred TestScenario1 {
  -- Eventually some process will be running.
  eventually some p: Process | isRunning[p]
		
  -- Eventually two (distinct) processes will be running at the same time.
  eventually some p1: Process | some p2 : Process - p1 | 
           	 isRunning[p1] and isRunning[p2]
  
  -- Eventually some process will be suspended.
  --  Note: Suspending a process is definitionally equal to 
	-- the the process being swapped out. As per the disambiguation found on Piazza,
	-- "You could interpreted the former [isSuspended" as saying that eventually the 
	-- suspend operator is applied to some process."
	-- Consequently we will check that the operator is legitimately applied here.
  eventually some p: Process | suspend[p]

  -- Eventually some process will be swapped out of memory.
  -- Note: See elaboration above.
  eventually some p: Process | isSwappedOut[p]

  -- Eventually some process will be terminated.
	eventually some p: Process | terminate[p]

  -- Eventually some process will be preempted.
  eventually some p: Process | preempt[p]

}

run TestScenario1 for 5 but 8 Resource, 12 Time

pred TestScenario2 {


  -- Two distinct processes are both inactive in the same state
  -- coming eventually after the initial one.
  -- Note: We're going to disambiguate this to mean
  -- that there exists two processes that are inactive at some point in our trace AFTER
  -- the Init step.
  eventually some p1: Process | some p2: Process - p1 |
  				 (not isInactive[p1] and not isInactive[p2])
				 and eventually (isInactive[p1] and isInactive[p2])


  -- Eventually some process will be blocked and then unblocked some time later.
  always some p: Process |
  				 -- To Unblock a process means either:
  				 -- The process will transition from Running -> (BlockedIn -> WaitingIn)
  				 eventually (p.status = BlockedIn and p.status' = WaitingIn)
				 -- The process will transition from Running -> BlockedIn -> (BlockedOut -> WaitingOut)
				 or eventually (p.status = BlockedOut and p.status' = WaitingOut)

  -- Any process that is running, at any time, is eventually terminated.
  always all p: Process | isRunning[p] => eventually (terminate[p])

  -- The scheduler repeatedly goes back to the initial state.
  always (Init until not Init) until Init

}

run TestScenario2 for 5 but 8 Resource, 12 Time

-----------------------
-- Expected Invariants
-----------------------

-- Inactive processes have nothing assigned to them
assert inactiveProcsHaveNothing {
 	always all p : Process | isInactive[p] =>
	no p.mem and
	no p.code and
	no p.proc and
	no p.res
}
check inactiveProcsHaveNothing for 5 but 10 Resource, 10 Time

-- No processor in the system runs more than one process
assert oneProcessPerProcessor {
  always all pr: Processor | lone proc.pr
}
check oneProcessPerProcessor for 5 but 10 Resource, 10 Time

-- A process runs exactly when it has an assigned processor.
--
-- Note: "exactly when", like "necessary and sufficient", is typically in English defined to mean
-- "If and only if". Consequently we'll use the iff operator `<=>` here.
assert runningProcessesAndProcessors { 
  always all p: Process | isRunning[p] <=> one p.proc
}
check runningProcessesAndProcessors for 5 but 10 Resource, 10 Time

-- No memory block is used both for code and for data
assert codeIsDisjointFromData { 
 always all m: MemBlock | one code.m => no mem.m and (one mem.m => no code.m)
}
check codeIsDisjointFromData for 4 but 6 Resource, 6 Time

-- A terminated process has no more resources of any kind associated to it
assert terminatedProcess { 
  always all p : Process | (p.status = Inactive and Track.op = Terminate) =>
	no p.mem and
	no p.code and
	no p.proc and
	no p.res

}
check terminatedProcess for 5 but 6 Resource, 10 Time

-- Unless an active process is swapped out, it is in memory
assert activeProcess { 
  -- Translating the term "Unless" from English to logic can be tricky.
  -- Here we're disambiguating it to "If you're active, you're either swapped out or you're in memory".
  always all p: Process | not isInactive[p] => isSwappedOut[p] or isInMemory[p]
}
check activeProcess for 4 but 6 Resource, 6 Time

-- Every process waiting for a File is blocked
assert waitingForFile { 
  always all p: Process | (one p.res and p.res in File) => isBlocked[p]
}
check waitingForFile for 4 but 6 Resource, 10 Time


-- A memory block containing data can be assigned to at most one process
assert dataMemoryIsNotShared { 
  always all m: MemBlock | lone mem.m
}
check dataMemoryIsNotShared for 4 but 6 Resource, 7 Time

-- Waiting processes do not have an assigned processor
assert waitingProcessesHaveNoProcessor { 
  always all p: Process | isWaiting[p] => no p.proc
}
check waitingProcessesHaveNoProcessor for 4 but 6 Resource, 10 Time


-- Processes that have associated memory for code have also 
-- associated memory for data
assert processesWithCodeHaveData { 
  always all p: Process | one p.code => one p.mem
}
check processesWithCodeHaveData for 4 but 6 Resource, 10 Time

-- Every currently blocked process was once running
assert BlockedOnlyIfOnceRunning { 
  always all p : Process | isBlocked[p] implies once isRunning[p]
}

check BlockedOnlyIfOnceRunning for 4 but 6 Resource, 10 Time

-- Every currently swapped out blocked process was once 
-- in memory and before that it was running
assert BlockedOnlyIfOnceRunning2 {
 -- This one is a bit tricky.
 -- First note that to be a "swapped out blocked process" is equivalent to "BlockedOut".
 -- So we say that if you're BlockedOut, then at some point you were in memory,
 -- and you've been in memory since you were running.
 always all p: Process | p.status = BlockedOut => ((once isInMemory[p]) since isRunning[p])
}
check BlockedOnlyIfOnceRunning2 for 4 but 6 Resource, 10 Time

-- Every swapped out process stays out indefinitely 
-- or until it goes back in memory
assert SwappedOutUntilBack { 
  always all p: Process | isSwappedOut[p] => (isSwappedOut[p'] or eventually isInMemory[p])
}
check SwappedOutUntilBack for 4 but 6 Resource, 10 Time

-- Only running processes can be terminated
assert onlyRunningProcessesGetTerminated {
 -- This assertion follows trivially from `terminate` having
 -- isRunning[p] in its preconditions.
 always all p: Process | terminate[p] => isRunning[p]
}
check onlyRunningProcessesGetTerminated for 4 but 6 Resource, 10 Time

---------------------------
-- Expected Non-Invariants
---------------------------

-- Distinct running processes can share code memory
assert runningProcesses { 
  -- Assertion: All distinct running processes can share code memory.
  -- Negation: All distinct running processes do not share code memory.
  -- Note: Electrum finds a counterexample to falsify this statement, as expected.
  always all p1 : Process | all p2: Process - p1 | (one p1.code and one p2.code) => p1.code != p2.code
}
check runningProcesses for 4 but 6 Resource, 12 Time

-- It is possible for a process to run indefinitely
assert runningProcessesIndefinitely {
 -- Assertion: There can exist a process that runs indefinitely.
 -- Negation: All running processes must stop running at some point.
 -- Note: Electrum is able to find a counterexample.
 always all p: Process | isRunning[p] => eventually not isRunning[p]
}
check runningProcessesIndefinitely for 4 but 6 Resource, 12 Time

-- It is possible for a process to be continually swapped in and out 
-- of memory forever
assert foreverSwaps { 
	-- Assertion: There exists a process such that, if that process is in memory, 
	--           it will next be suspended, and if it is swapped out, it will next be Resumed.
	-- Negation: For all processes, if that process is in memory, 
	--          it will not be suspended. And if that process is suspended, it will not be resumed.
	-- Note: The below produces a sufficient counter example: namely a process which is infinitely swapped in and out of memory.
	always all p : Process | (isInMemory[p] => not suspend[p]) and (isSuspended[p] => not some c, m: MemBlock | resume[p, c, m])

}
check foreverSwaps for 4 but 6 Resource, 12 Time

-- A waiting process may never get to run
assert waitingProcesses {
 -- Assertion: If a process is waiting, it may never transition to status = Running
 -- Negation: If a process is waiting, it MUST eventually transition to Running. 
 -- Note: Electrum is able to find a counter example.
 	always all p : Process | isWaiting[p] => eventually isRunning[p]
}
check waitingProcesses for 4 but 6 Resource, 12 Time

